package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;
@Service
public class Coursedao {

	@Autowired
	CourseRepository courseRepo;

	public List<Course> getCourses() {
		// TODO Auto-generated method stub
		return courseRepo.findAll();
	}

	public Course getCourseById(int cId) {
		// TODO Auto-generated method stub
		return courseRepo.findById(cId).orElse(null);
	}

	public Course getCourseByName(String cName) {
		
		return courseRepo.findByName(cName);
	}

	public Course addStudent(Course course) {
		// TODO Auto-generated method stub
		return courseRepo.save(course);
	}

	public Course updateStudent(Course course) {
		// TODO Auto-generated method stub
		return courseRepo.save(course);
	}

	public void deleteStudentById(int cId) {
		courseRepo.deleteById(cId);
		
	}
}