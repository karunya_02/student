package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student,Integer > {
	@Query("from Student  where studentName = :stdName")
	Student findByName(@Param("stdName") String stdName);
	
	@Query("from Student s where s.emailId = :emailId and s.password = :password")

	Student getLogin(@Param("emailId") String emailId, @Param("password")String password);

	
}