package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Course {
	@Id
	private int courseId;
	private String courseName;
	private double courseFee;
	
	@JsonIgnore
	@OneToMany(mappedBy="course")
	List<Student> empList = new ArrayList<Student>();

	public Course() {
		super();
	}

	public Course(int courseId, String courseName, double courseFee, List<Student> empList) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.courseFee = courseFee;
		this.empList = empList;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public double getCourseFee() {
		return courseFee;
	}

	public void setCourseFee(double courseFee) {
		this.courseFee = courseFee;
	}

	public List<Student> getEmpList() {
		return empList;
	}

	public void setEmpList(List<Student> empList) {
		this.empList = empList;
	}

	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", courseName=" + courseName + ", courseFee=" + courseFee + ", empList="
				+ empList + "]";
	}
	
}