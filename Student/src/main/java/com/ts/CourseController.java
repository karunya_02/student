package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.Coursedao;
import com.model.Course;
import com.model.Student;

@RestController
public class CourseController {

	@Autowired
	Coursedao coursedao;
	
	@GetMapping("getCourses")
	public List<Course> getCourses() {
		return coursedao.getCourses();
	}
	
	@GetMapping("getCourseById/{id}")
	public Course getCourseById(@PathVariable("id") int CId) {
		return coursedao.getCourseById(CId);
	}
	
	@GetMapping("getCourseByName/{name}")
	public Course getCourseByName(@PathVariable("name") String CName) {
		return coursedao.getCourseByName(CName);
	}
	
     @PostMapping("addCourse")
	public Course addCourse(@RequestBody Course course){
		return coursedao.addStudent(course);
	}
	@PutMapping("updateCourse")
	public  Course updateCourse(@RequestBody Course course){
		return coursedao.updateStudent(course);
	}
	
	@DeleteMapping("deleteCourseById/{id}")
	public String deleteStudentById(@PathVariable("id") int CId){
		coursedao.deleteStudentById(CId);
		return "Employee with EmployeeId: " +CId+ ", Deleted Successfully";
	}
}